package id.web.faerul.mvp.model;

/**
 * Created by faerulsalamun on 7/28/16.
 */
public class User {
    private String nama;

    public User(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
