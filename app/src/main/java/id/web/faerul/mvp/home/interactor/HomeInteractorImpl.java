package id.web.faerul.mvp.home.interactor;

import java.util.ArrayList;
import java.util.List;

import id.web.faerul.mvp.model.User;

/**
 * Created by faerulsalamun on 7/28/16.
 */
public class HomeInteractorImpl implements IHomeInteractor {

    @Override
    public List<User> getData() {
        List<User> userList = new ArrayList<>();

        userList.add(new User("test 1"));
        userList.add(new User("test 2"));

        return userList;

    }
}
