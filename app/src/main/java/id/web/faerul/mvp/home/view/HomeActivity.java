package id.web.faerul.mvp.home.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import id.web.faerul.mvp.R;
import id.web.faerul.mvp.home.presenter.HomePresenterImpl;
import id.web.faerul.mvp.model.User;

public class HomeActivity extends AppCompatActivity implements IHomeView {

    HomePresenterImpl homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Instansiasi dan inject presenter, biar presenter bisa ngendaliin view yang ada disini
        homePresenter = new HomePresenterImpl(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // Manggil data presenter
                homePresenter.getData();

            }
        });
    }

    @Override
    public void showData(List<User> data) {
        for (final User user : data) {
            Toast.makeText(this,user.getNama(),Toast.LENGTH_SHORT).show();
            Log.d("Nama user adalah",user.getNama());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
