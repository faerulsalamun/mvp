package id.web.faerul.mvp.home.presenter;

/**
 * Created by faerulsalamun on 7/28/16.
 */
public interface IHomePresenter<T> {
    void getData();
}
