package id.web.faerul.mvp.home.view;

import java.util.List;

import id.web.faerul.mvp.model.User;

/**
 * Created by faerulsalamun on 7/28/16.
 */
public interface IHomeView {
    void showData(List<User> data);
}
