package id.web.faerul.mvp.home.interactor;

import java.util.List;

import id.web.faerul.mvp.model.User;

/**
 * Created by faerulsalamun on 7/28/16.
 */
public interface IHomeInteractor<T> {
    List<User> getData();
}
