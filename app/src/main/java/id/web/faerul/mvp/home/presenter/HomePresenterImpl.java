package id.web.faerul.mvp.home.presenter;

import id.web.faerul.mvp.home.interactor.HomeInteractorImpl;
import id.web.faerul.mvp.home.view.IHomeView;

/**
 * Created by faerulsalamun on 7/28/16.
 */
public class HomePresenterImpl implements IHomePresenter {

    IHomeView iHomeView;
    HomeInteractorImpl homeInteractor;

    public HomePresenterImpl(IHomeView iHomeView) {
        this.iHomeView = iHomeView;

        // Instansiasi interactor, biar presenter bisa ngendaliin interactor
        this.homeInteractor = new HomeInteractorImpl();
    }

    @Override
    public void getData() {
        // Ambil data dari ke interactor terus dikasih view
        // Disini juga bisa ditambahin logic, jadi jangan sebisa mungkin logic jangan di gunakan di view
        iHomeView.showData(homeInteractor.getData());
    }
}
